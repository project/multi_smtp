
Overview
========
Allows admins to manage multiple SMTP servers. A server is randomly chosen for each page request, so that all mail going out of the system is spread across the servers. The GET variable `use_smtp_id` can be set with the server ID to use for the request.

This module sits on top of the SMTP module to add the ability to manage multiple servers. All mail handling is still done by the SMTP module, which makes use of PHPMailer. PHPMailer supports using SSL or TLS encryption as long as the OpenSSL functions are available in your installation of PHP. More information about PHPMailer can be found here: http://phpmailer.worxware.com/

Usage
=====
* Install the module normally.
* If you had already configured an SMTP server in the regular SMTP module, it will be imported into the settings for Multi SMTP.
* If not, navigate to `admin/settings/multi_smtp` and add some servers.
* Make sure you visit the Settings tab to enable SMTP support. This option is a holdover from the regular SMTP module.

Requirements
============
* SMTP Authentication Support module: http://drupal.org/project/smtp

Roadmap
=======
I've got a few things I'd like to add to this module in the future:

* Make the settings exportable using Ctools, so that it can be included in Features modules. I haven't looked into this yet.
* Add an option to disable a server without deleting it.