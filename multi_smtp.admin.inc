<?php

/**
 * @file
 * Admin options for SMTP servers
 */

/**
 * Show list of SMTP servers that have been configured
 */
function multi_smtp_server_list() {
  // If SMTP support isn't enabled, warn the user
  if (!variable_get('smtp_on', 0)) {
    drupal_set_message(t('Note that SMTP support needs to be enabled on the Settings tab before your servers will be used.'), 'warning');
  }

  $rows = array();
  $header = array(t('Server Nickname'), t('Host Name/IP'), t('Status'), t('Operations'));

  // Get all the SMTP servers and add them to an array to be themed as a table
  $sql = 'SELECT smtp_id, smtp_nickname, smtp_host, wait_timestamp FROM {multi_smtp_servers} ORDER BY smtp_nickname';
  $result = db_query($sql);
  while ($row = db_fetch_object($result)) {
    $operations = l(t('edit'), 'admin/settings/multi_smtp/edit/'. $row->smtp_id);
    $operations .= '&nbsp;&nbsp;' . l(t('delete'), 'admin/settings/multi_smtp/delete/'. $row->smtp_id);
    if ($row->wait_timestamp > 0) {
      // Add link for re-activating a server.
      $operations .= '&nbsp;&nbsp;' . l(t('re-activate'), 'admin/settings/multi_smtp/reactivate/' . $row->smtp_id);
    }

    // If the server has a wait_timestamp value, it is currently inactive
    if ($row->wait_timestamp > 0) {
      $status = t('On hold until ') . format_date($row->wait_timestamp);
    }
    else {
      $status = t('Active');
    }

    $rows[] = array(
      check_plain($row->smtp_nickname),
      check_plain($row->smtp_host),
      $status,
      $operations
    );
  }

  if (count($rows) == 0) {
    $rows[] = array(array('data' => t('No SMTP servers have been created. Click Add Server above to add your first.'), 'colspan' => count($header)));
  }
  return theme('table', $header, $rows);
}


/**
 * Global settings form for SMTP usage.
 * 
 * This form replicates the basic SMTP options from the SMTP module that are
 * *not* specific to a single server.
 */
function multi_smtp_settings(&$form_state) {
  $form = array();
  $form['smtp_on'] = array(
    '#type'          => 'radios',
    '#title'         => t('Turn this module on or off'),
    '#default_value' => variable_get('smtp_on', 0),
    '#options'       => array(1 => t('On'), 0 => t('Off')),
    '#description'   => t('To uninstall this module you must turn it off here first.'),
  );

  $form['smtp_debugging'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable debugging'),
    '#default_value' => variable_get('smtp_debugging', 0),
    '#description'   => t("Checking this box will print SMTP messages from the server for every e-mail that is sent. This should never be enabled for production usage; it results in so much information that it should only be used when something isn't working."),
  );

  return system_settings_form($form);
}


/**
 * Admin form to add/edit SMTP server
 */
function multi_smtp_server_form(&$form_state, $server_id = 0) {
  $server_info = array();
  if (isset($server_id)) {
    $form_state['server_id'] = $server_id;
    $result = db_query('SELECT * FROM {multi_smtp_servers} WHERE smtp_id=%d', $server_id);
    while ($row = db_fetch_array($result)) {
      $server_info = $row;
    }
  }

  $form['server_details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server Details'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );
  $form['server_details']['smtp_nickname'] = array(
    '#type' => 'textfield',
    '#title' => t('Server Nickname'),
    '#description' => t('A human-friendly name for this server'),
    '#default_value' => $server_info['smtp_nickname'],
    '#required' => TRUE,
    '#size' => 40,
    '#maxlength' => 255,
  );
  $form['server_details']['smtp_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host Name'),
    '#description' => t('Server name or IP address'),
    '#default_value' => $server_info['smtp_host'],
    '#required' => TRUE,
    '#size' => 40,
    '#maxlength' => 255,
  );
  $form['server_details']['smtp_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#description'   => t('The default SMTP port is 25, if that is being blocked try 80. Gmail uses 465. See !url for more information on configuring for use with Gmail.', array('!url' => l(t('this page'), 'http://gmail.google.com/support/bin/answer.py?answer=13287'))),
    '#default_value' => ($server_info['smtp_port'] > 0) ? $server_info['smtp_port'] : 25,
    '#required' => TRUE,
    '#size' => 40,
    '#maxlength' => 255,
  );

  // Only display the Encryption option if openssl is installed.
  // This part copied from SMTP module
  if (function_exists('openssl_open')) {
    $encryption_options = array(
      'standard' => t('No'),
      'ssl' => t('Use SSL'),
      'tls' => t('Use TLS'),
    );
    $encryption_description = t('This allows connection to an SMTP server that requires SSL encryption such as Gmail.');
  }
  // If openssl is not installed, use normal protocol.
  else {
    variable_set('smtp_protocol', 'standard');
    $encryption_options = array('standard' => t('No'));
    $encryption_description = t('Your PHP installation does not have SSL enabled. See the !url page on php.net for more information. Gmail requires SSL.', array('!url' => l(t('OpenSSL Functions'), 'http://php.net/openssl')));
  }
  $form['server_details']['smtp_protocol'] = array(
    '#type' => 'select',
    '#title' => t('Use encrypted protocol'),
    '#default_value' => ($server_info['smtp_protocol']) ? $server_info['smtp_protocol'] : 'standard',
    '#options' => $encryption_options,
    '#description' => $encryption_description,
  );


  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credentials'),
    '#description' => t('Leave blank if your SMTP server does not require authentication.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );
  $form['credentials']['smtp_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Username for the server'),
    '#default_value' => $server_info['smtp_username'],
    '#size' => 40,
    '#maxlength' => 255,
  );
  $form['credentials']['smtp_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('Password for the server'),
    '#default_value' => '',
    '#size' => 30,
    '#maxlength' => 255,
  );

  $form['email_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail Options'),
    '#description' => t('The From address will be used for most mail out of the system. Note that Simplenews uses its own newsletter-level settings for the From address, and other modules may similarly override this value on e-mail.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );
  $form['email_options']['smtp_from'] = array(
    '#type' => 'textfield',
    '#title' => t('From Address'),
    '#description' => t('From address to use for e-mails sent through this server'),
    '#default_value' => $server_info['smtp_from'],
    '#size' => 40,
    '#maxlength' => 255,
  );
  $form['email_options']['smtp_fromname'] = array(
    '#type' => 'textfield',
    '#title' => t('From Name'),
    '#description' => t('Name on from address to use for e-mails sent through this server'),
    '#default_value' => $server_info['smtp_fromname'],
    '#size' => 40,
    '#maxlength' => 255,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  // Return to the list after submitting
  $form['#redirect'] = 'admin/settings/multi_smtp/list';

  return $form;
}


/**
 * Validate fields in the server settings
 */
function multi_smtp_server_form_validate($form, &$form_state) {
  if ($form_state['values']['smtp_from'] && !valid_email_address($form_state['values']['smtp_from'])) {
    form_set_error('smtp_from', t("The sender's email address you supplied is not valid."));
  }

  // Check the SMTP port
  if ($form_state['values']['smtp_port']) {
    // Make sure it's a number, and an int
    if (!is_numeric($form_state['values']['smtp_port']) ||
      (int)$form_state['values']['smtp_port'] != $form_state['values']['smtp_port']) {
        form_set_error('smtp_port', t('You must enter an integer for the SMTP port number.'));
    }
  }

  if ($form_state['values']['smtp_from'] && !valid_email_address($form_state['values']['smtp_from'])) {
    form_set_error('smtp_from', t('The provided from e-mail address is not valid.'));
  }
}


/**
 * Submit the server add/edit form
 */
function multi_smtp_server_form_submit($form, &$form_state) {
  $server_id = $form_state['server_id'];

  // Build an object to save to the DB
  $record = new stdClass();
  $record->smtp_id = $server_id;
  $record->smtp_nickname = check_plain($form_state['values']['smtp_nickname']);
  $record->smtp_host = check_plain($form_state['values']['smtp_host']);
  $record->smtp_port = check_plain($form_state['values']['smtp_port']);
  $record->smtp_protocol = check_plain($form_state['values']['smtp_protocol']);
  $record->smtp_username = check_plain($form_state['values']['smtp_username']);
  $record->smtp_from = check_plain($form_state['values']['smtp_from']);
  $record->smtp_fromname = check_plain($form_state['values']['smtp_fromname']);

  // Only update the password if one was provided: don't want to overwrite a
  // password that was set
  if ($form_state['values']['smtp_password']) {
    $record->smtp_password = $form_state['values']['smtp_password'];
  }


  // Updating an existing server...
  if ($server_id > 0) {
    if (drupal_write_record('multi_smtp_servers', $record, 'smtp_id')) {
      drupal_set_message(t('SMTP server details updated. Be sure to send a !test_message to confirm that your settings are correct.', array('!test_message' => l(t('test message'), 'admin/settings/multi_smtp/test'))));
    }
    else {
      drupal_set_message(t('Error updating SMTP server'), 'error');
    }
  }
  // Adding a new server
  else {
    if (drupal_write_record('multi_smtp_servers', $record)) {
      drupal_set_message(t('SMTP server created. Be sure to send a !test_message to confirm that your settings are correct.', array('!test_message' => l(t('test message'), 'admin/settings/multi_smtp/test'))));
    }
    else {
      drupal_set_message(t('Error creating SMTP server'), 'error');
    }
  }
}


/**
 * Delete action. If the given server ID is valid, display the confirmation form
 * 
 * @param $server_id
 *   ID of the server to delete.
 * 
 * @return
 *   If the given $server_id is valid, a delete confirmation return is returned.
 */
function multi_smtp_server_delete($server_id = NULL) {
  $server_details = db_fetch_object(db_query('SELECT smtp_id, smtp_nickname, smtp_host FROM {multi_smtp_servers} WHERE smtp_id = %d', $server_id));

  if (is_object($server_details) && is_numeric($server_details->smtp_id)) {
    return drupal_get_form('multi_smtp_server_confirm_delete', $server_details);
  }
  else {
    drupal_set_message(t('That server does not exist.'), 'error');
    drupal_goto('admin/settings/multi_smtp');
  }
}


/**
 * Build the confirmation form that confirms the user wants to delete an
 * SMTP server
 */
function multi_smtp_server_confirm_delete(&$form_state, $server_details) {
  $form = array();
  $form['#server'] = $server_details;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the server %smtp_nickname (%smtp_host)?', array('%smtp_nickname' => $server_details->smtp_nickname, '%smtp_host' => $server_details->smtp_host)),
    'admin/settings/multi_smtp',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'),
    'multi_smtp_server_confirm_delete');
}

/**
 * Process multi_smtp_server_confirm_delete form submissions.
 */
function multi_smtp_server_confirm_delete_submit($form, &$form_state) {
  $server_details = $form['#server'];
  db_query('DELETE FROM {multi_smtp_servers} WHERE smtp_id = %d', $server_details->smtp_id);
  drupal_set_message(t('The server has been deleted.'));
  $form_state['redirect'] = "admin/settings/multi_smtp";
}

/**
 * Re-activates a given server ID.
 *
 * If the given server ID is valid, load the server details and return the
 * confirmation form. Otherwise, display error message and return to list.
 */
function multi_smtp_server_reactivate($server_id = NULL) {
  $server_details = db_fetch_object(db_query('SELECT smtp_id, smtp_nickname, smtp_host FROM {multi_smtp_servers} WHERE smtp_id = %d', $server_id));

  if (is_object($server_details) && is_numeric($server_details->smtp_id)) {
    return drupal_get_form('multi_smtp_server_confirm_reactivate', $server_details);
  }
  else {
    drupal_set_message(t('That server does not exist.'), 'error');
    drupal_goto('admin/settings/multi_smtp');
  }
}

/**
 * Builds the confirmation form for re-activating a given SMTP server.
 */
function multi_smtp_server_confirm_reactivate(&$form_state, $server_details) {
  $form = array();
  $form['#server'] = $server_details;
  return confirm_form(
    $form,
    t('Are you sure you want to re-activate the server %smtp_nickname (%smtp_host)?', array('%smtp_nickname' => $server_details->smtp_nickname, '%smtp_host' => $server_details->smtp_host)),
    'admin/settings/multi_smtp',
    t('This action cannot be undone.'),
    t('Re-activate'),
    t('Cancel'),
    'multi_smtp_server_confirm_reactivate');
}

/**
 * Processes multi_smtp_server_confirm_reactivate form submissions.
 */
function multi_smtp_server_confirm_reactivate_submit($form, &$form_state) {
  drupal_set_message(t('The server has been re-activated.'));

  $server_details = $form['#server'];

  db_query('UPDATE {multi_smtp_servers} SET wait_timestamp = 0 WHERE smtp_id = %d', $server_details->smtp_id);

  $form_state['redirect'] = "admin/settings/multi_smtp";
}

/**
 * Form to send a test message on a server
 *
 * Takes an e-mail address and a selection of a server, and sends a test e-mail
 * from that server to that address
 */
function multi_smtp_server_test(&$form_state) {
  $form = array();
  $form['test_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail Address'),
    '#description' => t('E-mail address to send the test message to'),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE
  );

  $servers = multi_smtp_get_all_servers();

  // If no servers have been setup, you can't send a test message
  if (empty($servers)) {
    drupal_set_message(t('No SMTP servers have been configured, so no test message can be sent. Click on the Add Server tab to setup a server.'), 'error');
    return '';
  }

  $server_options = array('' => t('Choose one'));
  foreach ($servers as $server_id => $server_details) {
    $server_options[$server_id] = t('@smtp_nickname (@smtp_host)', array('@smtp_nickname' => $server_details['smtp_nickname'], '@smtp_host' => $server_details['smtp_host']));
  }
  $form['server'] = array(
    '#type' => 'select',
    '#title' => t('Server'),
    '#options' => $server_options,
    '#default_value' => -1,
    '#required' => TRUE
  );
  $form['send'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}


/**
 * Validate the test send form
 */
function multi_smtp_server_test_validate($form, &$form_state) {
  if ($form_state['values']['test_email'] && !valid_email_address($form_state['values']['test_email'])) {
    form_set_error('test_email', t("Enter a valid e-mail address."));
  }
}

/**
 * Send a test message
 */
function multi_smtp_server_test_submit($form, &$form_state) {
  // If an address was given, send a test e-mail message.
  $test_address = $form_state['values']['test_email'];
  multi_smtp_load_vars($form_state['values']['server']);
  global $language;
  $params['subject'] = t('Drupal test e-mail');
  $params['body']    = t('If you receive this message it means your site is capable of sending e-mail. Sent from server @smtp_nickname (@smtp_host)', array('@smtp_nickname' => variable_get('smtp_nickname', ''), '@smtp_host' => variable_get('smtp_host', '')));
  drupal_mail('smtp', 'smtp-test', $test_address, $language, $params);
  drupal_set_message(t('A test e-mail has been sent to @email. You may want to !check for any error messages.', array('@email' => $test_address, '!check' => l(t('check the logs'), 'admin/reports/dblog'))));
}